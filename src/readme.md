ionic start .
npm install --save @capacitor/core @capacitor/cli
npm install --save @ionic/pwa-elements
npx cap init
ionic build
npx cap add ios
npx cap add android
npx cap add electron

add to index.html
<script src="https://unpkg.com/@ionic/pwa-elements@1.0.0/dist/ionicpwaelements.js"></script>

Running on ios -- will open xcode
1) ionic cap run ios -l
 or
2) npx cap open ios


Running on android -- will open android developer
1) ionic cap run android -l
 or
2) npx cap open android

Maybe use
1) npx cap copy


https://github.com/ionic-team/capacitor/issues/863

ios
app/info.plist

android
AndroidManifest.xml
==========
Capacitor intro: https://ionicpro.wistia.com/medias/rukefbicc1?wvideo=rukefbicc1


