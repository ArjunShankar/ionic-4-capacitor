import { Component } from '@angular/core';
import { Capacitor, Plugins } from '@capacitor/core';

@Component({
  selector: 'app-contact',
  templateUrl: 'contact.page.html',
  styleUrls: ['contact.page.scss']
})
export class ContactPage {

  ngOnInit() {

    this.onLoad();
  }

    onLoad() {
      try {
          if (Capacitor.isPluginAvailable('LocalNotifications')) {
              this.showLocalNotification();
          } else {
              this.showToast();
          }
      } catch (error) {
          this.showToast();
      }
  }

    async showToast() {
        const { Toast } = Plugins;

        await Toast.show({
            text: 'Welcome! Please update your picture on the Account page.',
            duration: 'long'
        });
    }

    async showLocalNotification() {
        const { LocalNotifications } = Plugins;

        LocalNotifications.schedule({
            notifications: [
                {
                    title: 'Welcome!',
                    body: 'Time to update your profile picture!',
                    id: 1234,
                    schedule: { at: new Date(Date.now() + 3000 )},
                    sound: null,
                    attachments: null,
                    actionTypeId: 'OPEN_ACCOUNT_PAGE',
                    extra: null
                }
            ]
        });
    }
}
