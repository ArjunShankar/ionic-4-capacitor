import { Component } from '@angular/core';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-about',
  templateUrl: 'about.page.html',
  styleUrls: ['about.page.scss']
})
export class AboutPage {

    userProfilePic: SafeResourceUrl;

    constructor(
        private sanitizer: DomSanitizer) {
    }

    async updatePicture() {
        const { Camera } = Plugins;

        const image = await Camera.getPhoto({
            quality: 90,
            allowEditing: false,
            resultType: CameraResultType.Base64,
            source: CameraSource.Camera
        });

        // Example of using the Base64 return type. It's recommended to use CameraResultType.Uri
        // instead for performance reasons when showing large, or a large amount of images.
        this.userProfilePic = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.base64Data));

        console.log('User profile pic has been updated!');
    }

}