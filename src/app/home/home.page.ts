import {Component, OnInit} from '@angular/core';
import {Plugins} from '@capacitor/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit{

    public device_data: any;

    ngOnInit() {

        this.showDeviceData();
    }

    async showDeviceData() {
        const { Device } = Plugins;

        const device_data = await Device.getInfo();

        console.log(device_data);

    }

}
