import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-long-list',
  templateUrl: './long-list.page.html',
  styleUrls: ['./long-list.page.scss'],
})
export class LongListPage implements OnInit {

  public items: Array<any>;
 // public vbox:

  constructor() {
    this.items = [];
  }

  ngOnInit() {
    for (let i = 1; i <= 2000; i++) {
      this.items.push( {
        label: i,
        value: i % 2 === 0 ? true : false
      });
    }
  }


  handleClick(evt, index) {
    console.log(evt, index);
  }
//  vbox:
}
