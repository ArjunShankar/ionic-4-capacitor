import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LongListPage } from './long-list.page';

describe('LongListPage', () => {
  let component: LongListPage;
  let fixture: ComponentFixture<LongListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LongListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LongListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
